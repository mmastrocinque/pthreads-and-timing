### pThreads and Timing ###

An example project that demonstrates the fundamentals of creating pThreads along with taking timing measurements.

To compile and run from command line:
```
g++ pThreads_Timing.cpp CStopWatch.cpp
./a.out
```
or
```
   cd Default && make all
```

To compile and run with Docker:
```
docker run --rm -v ($pwd):/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all
docker run --rm -v ($pwd):/tmp -w /tmp/Default rgreen13/alpine-bash-bpp ./pThreads_Timing
```
